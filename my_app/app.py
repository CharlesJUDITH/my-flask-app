#!/usr/bin/env python3
"""
Example app for metric instrumentation with Prometheus.
"""
import random
import time
import prometheus_client
from flask import Response, Flask, render_template
from prometheus_client import Counter, Histogram, Info, Summary


def create_app():
    """
    The magic happens here.
    """
    app = Flask(__name__)

    @app.route('/metrics/')
    def metrics():
        """
        Metrics endpoint
        """
        return Response(
            prometheus_client.generate_latest(),
            mimetype='text/plain; version=0.0.4; charset=utf-8'
        )

    REQUESTS = Counter(
        'requests', 'Application Request Count',
        ['endpoint']
    )

    @app.route('/')
    def index():
        """
        Render "/index.html" page
        """
        REQUESTS.labels(endpoint='/').inc()
        return render_template("index.html")

    TIMER = Histogram(
        'slow', 'Slow Requests',
        ['endpoint']
    )

    @app.route('/database/')
    def database():
        """
        Database metric simulation
        """
        with TIMER.labels('/database').time():
            time.sleep(random.uniform(1, 3))
        return render_template("database.html")

    # Create a metric to track time spent and requests made.
    REQUEST_TIME = Summary('request_processing_seconds', 'Time spent processing request')

    # Decorate function with metric.
    @REQUEST_TIME.time()
    def process_request(t):
        """A dummy function that takes some time."""
        time.sleep(t)

    # Add app info as a metric
    i = Info('my_build_version', 'Description of MyApp')
    i.info({'version': '9.7.4', 'buildhost': 'super@computeur'})

    # Add app failure metric
    c = Counter('my_failures', 'Description of counter')
    c.inc()  # Increment by 1
    c.inc(1.6)  # Increment by given value

    process_request(random.random())

    return app


if __name__ == "__main__":
    flask_app = create_app()
    flask_app.run()
