import pytest
from my_app.app import create_app


@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    with app.test_client() as client:
        yield client


def test_home_page(client):
    response = client.get('/')
    assert response.status_code == 200
    assert b"Welcome" in response.data
    assert b"instrumentation" in response.data


#def test_database(client):
#    response = client.get('/database')
#    assert response.status_code == 200
    #assert b"simulates" in response.data
    #assert b"database" in response.data


#def test_process_request(client):
    #response = client.get('/metrics')
    #assert response.status_code == 200
    #assert b"HELP" in response.data
    #assert b"TYPE" in response.data
